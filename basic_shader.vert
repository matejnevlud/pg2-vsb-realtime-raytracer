#version 410 core
#define PI 3.1415926538

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texcoord;

layout (location = 4) in vec3 ambient;
layout (location = 5) in vec3 diffuse;
layout (location = 6) in vec3 specular;

layout (location = 7) in float roughness;


uniform vec3 Eye;

uniform mat4 M;
uniform mat4 MV;
uniform mat4 MVN;
uniform mat4 MVP;

out vec4 wPosition;
out vec3 Position;
out vec3 Normal;
out vec3 Color;
out vec2 TexCoord;

out vec3 Ambient;
out vec3 Diffuse;
out vec3 Specular;

out float Roughness;

void main( void )
{
	vec4 position_vec4 = vec4( position, 1.0 );
	wPosition = MVP * position_vec4;
	gl_Position = wPosition;

	//vec4 eye = MV * vec4(position, 1.0);

	// Flip Normal if pointing inwards
	vec3 omega_o = normalize(Eye.xyz - position.xyz);
	float cos_theta_o = dot(normal, omega_o);
	Normal = (cos_theta_o < 0.0) ? -normal : normal;
	
	// Unified Normal ???
	//Normal = vec3(MVN * vec4(Normal, 0.0));

	Color = color;
	Position = position;
	TexCoord = texcoord;

	Ambient = ambient;
	Diffuse = diffuse;
	Specular = specular;

	Roughness = roughness;
	
}
