#include "pch.h"
#include "tutorials.h"
#include "rasterizer.h"

int main()
{
	printf( "PG2 OpenGL, (c)2019-2021 Tomas Fabian\n\n" );

	//return tutorial_2();
	Rasterizer rasterizer = Rasterizer(1280, 720);
	rasterizer.InitDevice();
	rasterizer.InitPrograms();
	rasterizer.InitIrradianceMap("./data/maps/lebombo_irradiance_map.exr");
	rasterizer.InitGGXIntegrMap("./data/maps/brdf_integration_map_ct_ggx.png");
	rasterizer.InitPrefilteredEnvMap( {
		"./data/maps/lebombo_prefiltered_env_map_001_2048.exr",
		"./data/maps/lebombo_prefiltered_env_map_010_1024.exr",
		"./data/maps/lebombo_prefiltered_env_map_100_512.exr",
		"./data/maps/lebombo_prefiltered_env_map_250_256.exr",
		"./data/maps/lebombo_prefiltered_env_map_500_128.exr",
		"./data/maps/lebombo_prefiltered_env_map_750_64.exr",
		"./data/maps/lebombo_prefiltered_env_map_999_32.exr" });
	//rasterizer.LoadScene("./data/piece_02/piece_02.obj");
	rasterizer.LoadScene("./data/6887_allied_avenger/6887_allied_avenger_gi2.obj");
	//rasterizer.LoadScene("./data/test_scene_whitted/test_scene_whitted.obj");
	rasterizer.MainLoop();


}
