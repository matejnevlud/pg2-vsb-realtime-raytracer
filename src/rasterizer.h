#include "pch.h"
#include "utils.h"
#include "camera.h"
#include "camera.h"
#include "objloader.h"


class Rasterizer {
	private:
		int width;
		int height;

		GLFWwindow* window;

		GLuint vao = 0;
		GLuint vbo = 0;
		GLuint ebo = 0; // optional buffer of indices

		int no_of_verticies = 0;

		GLuint vertex_shader;
		GLuint fragment_shader;
		GLuint shader_program;


		void framebuffer_resize_callback( GLFWwindow * window, int width, int height );

		static void mouse_callback(GLFWwindow* window, double xpos, double ypos);

		char * LoadShader( string file_name);
		GLint CheckShader( const GLenum shader);

	public:
		Rasterizer(const int _width, const int _height);
		~Rasterizer();

		int InitDevice(); // inicializace OpenGL kontextu
		void InitPrograms(); // inicializace VS a FS shaderů (programu) 

		void LoadScene( string file_name ); // načtení geometrie scény 
		void LoadSceneDummy( string file_name ); // načtení geometrie scény 

		void InitBuffers(); // inicializace VAO a VBO 
		void InitMaterials( int arg ); // inicializace SSBO pro uložení materiálů 
		
		void InitIrradianceMap( string file_name ); // inicializace předpočítaných map pro Cook-Torrance GGX shader 
		void InitPrefilteredEnvMap( vector<string> file_names ); 
		void InitGGXIntegrMap( string file_names ); 

		int MainLoop(); 
};

Rasterizer::Rasterizer(const int _width, const int _height) {
	width = _width;
	height = _height;
}

Rasterizer::~Rasterizer() {

}


void Rasterizer::framebuffer_resize_callback( GLFWwindow * window, int width, int height )
{
	glViewport( 0, 0, width, height );
}

/* invoked when window is resized */
void framebuffer_resize_clb( GLFWwindow * window, int width, int height ) {
	glViewport( 0, 0, width, height );
}

void GLAPIENTRY opengl_callback( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * user_param )
{
	printf( "GL %s type = 0x%x, severity = 0x%x, message = %s\n",
		( type == GL_DEBUG_TYPE_ERROR ? "Error" : "Message" ),
		type, severity, message );
}


/* load shader code from text file */
char * Rasterizer::LoadShader( string file_name ) {
	FILE * file = fopen( file_name.c_str(), "rt" );

	if ( file == NULL ) {
		printf( "IO error: File '%s' not found.\n", file_name.c_str() );
		return NULL;
	}

	size_t file_size = static_cast< size_t >( GetFileSize64( file_name.c_str() ) );
	char * shader = NULL;

	if ( file_size < 1 ) {
		printf( "Shader error: File '%s' is empty.\n", file_name.c_str() );
	}
	else
	{
		/* v glShaderSource nezad�v�me v posledn�m parametru d�lku,
		tak�e �et�zec mus� b�t null terminated, proto +1 a reset na 0*/
		shader = new char[file_size + 1];
		memset( shader, 0, sizeof( *shader ) * ( file_size + 1 ) );

		size_t bytes = 0; // po�et ji� na�ten�ch byt�

		do
		{
			bytes += fread( shader, sizeof( char ), file_size, file );
		} while ( !feof( file ) && ( bytes < file_size ) );

		if ( !feof( file ) && ( bytes != file_size ) )
		{
			printf( "IO error: Unexpected end of file '%s' encountered.\n", file_name.c_str() );
		}
	}

	fclose( file );
	file = NULL;

	return shader;
}

/* check shader for completeness */
GLint Rasterizer::CheckShader( const GLenum shader )
{
	GLint status = 0;
	glGetShaderiv( shader, GL_COMPILE_STATUS, &status );

	printf( "Shader compilation %s.\n", ( status == GL_TRUE ) ? "was successful" : "FAILED" );

	if ( status == GL_FALSE )
	{
		int info_length = 0;
		glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &info_length );
		char * info_log = new char[info_length];
		memset( info_log, 0, sizeof( *info_log ) * info_length );
		glGetShaderInfoLog( shader, info_length, &info_length, info_log );

		printf( "Error log: %s\n", info_log );

		SAFE_DELETE_ARRAY( info_log );
		exit(0);
	}

	return status;
}


void Rasterizer::InitIrradianceMap( string file_name ) {
	Texture3f irradianceMap = Texture3f(file_name);

	
	GLuint irradianceMap_id;
	glGenTextures(1, &irradianceMap_id);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, irradianceMap_id);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, irradianceMap.width(), irradianceMap.height(), 0, GL_RGB, GL_FLOAT, irradianceMap.data());
	glUniform1i(glGetUniformLocation(shader_program, "irradianceMap"), 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	
}

void Rasterizer::InitGGXIntegrMap( string file_name ) {
	Texture3f ggxMap = Texture3f(file_name);

	
	GLuint ggxMap_id;
	glGenTextures(1, &ggxMap_id);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, ggxMap_id);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, ggxMap.width(), ggxMap.height(), 0, GL_RGB, GL_FLOAT, ggxMap.data());
	glUniform1i(glGetUniformLocation(shader_program, "ggxMap"), 1);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	
}


void Rasterizer::InitPrefilteredEnvMap( vector<string> file_names ) {

	GLuint environmentMap_id;

	const GLint max_level = GLint( file_names.size() ) - 1; // assume we have a list of images representing different levels of a map
	glGenTextures( 1, &environmentMap_id ); 
	glActiveTexture(GL_TEXTURE2);
	glBindTexture( GL_TEXTURE_2D, environmentMap_id ); 
	if ( glIsTexture( environmentMap_id ) ) {
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT ); 
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT ); 
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR ); 
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0 ); 
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, max_level );
		int width, height; 
		GLint level;
		for ( level = 0; level < GLint( file_names.size() ); ++level ) {
			Texture3f prefiltered_env_map = Texture3f( file_names[level] );
			// for HDR images use GL_RGB32F or GL_RGB16F as internal format !!!
			glTexImage2D( GL_TEXTURE_2D, level, GL_RGB32F, prefiltered_env_map.width(), prefiltered_env_map.height(), 0, GL_RGB, GL_FLOAT,
			prefiltered_env_map.data() );
			width = prefiltered_env_map.width() / 2;
			height = prefiltered_env_map.height() / 2; 
		}
	}

	glUniform1i(glGetUniformLocation(shader_program, "environmentMap"), 2);

	

}


int Rasterizer::InitDevice() {

	//glfwSetErrorCallback( glfw_callback );

	if ( !glfwInit() )
	{
		return( EXIT_FAILURE );
	}

	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 1 );

	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
	glfwWindowHint( GLFW_SAMPLES, 8 );
	glfwWindowHint( GLFW_RESIZABLE, GL_TRUE );
	glfwWindowHint( GLFW_DOUBLEBUFFER, GL_TRUE );	
	

	window = glfwCreateWindow( width, height, "PG2 OpenGL", nullptr, nullptr );
	if ( !window )
	{
		glfwTerminate();
		return EXIT_FAILURE;
	}
	
	glfwSetFramebufferSizeCallback( window, framebuffer_resize_clb );
	
	glfwMakeContextCurrent( window );

	if ( !gladLoadGLLoader( ( GLADloadproc )glfwGetProcAddress ) )
	{
		if ( !gladLoadGL() )
		{
			return EXIT_FAILURE;
		}
	}
	
	glEnable( GL_DEBUG_OUTPUT );
	//glDebugMessageCallback( opengl_callback, nullptr );
	
	printf( "OpenGL %s, ", glGetString( GL_VERSION ) );
	printf( "%s", glGetString( GL_RENDERER ) );
	printf( " (%s)\n", glGetString( GL_VENDOR ) );
	printf( "GLSL %s\n", glGetString( GL_SHADING_LANGUAGE_VERSION ) );	

	check_gl();

	glEnable( GL_MULTISAMPLE );
	
	// map from the range of NDC coordinates <-1.0, 1.0>^2 to <0, width> x <0, height>
	// Fix for retina display ie. 2x ui size
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);
	// GL_LOWER_LEFT (OpenGL) or GL_UPPER_LEFT (DirectX, Windows) and GL_NEGATIVE_ONE_TO_ONE or GL_ZERO_TO_ONE
	//glClipControl( GL_LOWER_LEFT, GL_ZERO_TO_ONE );


	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);   

}

void Rasterizer::InitPrograms() {

	vertex_shader = glCreateShader( GL_VERTEX_SHADER );
	const char * vertex_shader_source = LoadShader( "basic_shader.vert" );
	glShaderSource( vertex_shader, 1, &vertex_shader_source, nullptr );
	glCompileShader( vertex_shader );
	SAFE_DELETE_ARRAY( vertex_shader_source );
	CheckShader( vertex_shader );
	
	fragment_shader = glCreateShader( GL_FRAGMENT_SHADER );
	const char * fragment_shader_source = LoadShader( "basic_shader.frag" );
	glShaderSource( fragment_shader, 1, &fragment_shader_source, nullptr );
	glCompileShader( fragment_shader );
	SAFE_DELETE_ARRAY( fragment_shader_source );
	CheckShader( fragment_shader );

	shader_program = glCreateProgram();
	glAttachShader( shader_program, vertex_shader );
	glAttachShader( shader_program, fragment_shader );
	glLinkProgram( shader_program );
	// TODO check linking
	glUseProgram( shader_program );


	glPointSize( 10.0f );	
	glLineWidth( 2.0f );
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );


	// TODO Dopsano přednáška 5
	// Vypnutí vykreslování trianglů, které nemůžou být viděny z pozice camery
	glFrontFace( GL_CCW );
	glCullFace( GL_BACK );
	glDisable( GL_CULL_FACE );
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LESS );

	glProvokingVertex( GL_LAST_VERTEX_CONVENTION );


	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

}




void Rasterizer::LoadSceneDummy(string file_name) {


	static const GLfloat vertices_cube[] = {
		-1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	-1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		-1.0f,	 1.0f,	 1.0f,			0.0f,	 1.0f,
		1.0f,	-1.0f,	 1.0f,			0.0f,	 1.0f,
	};

	// setup vertex buffer as AoS (array of structures)
	GLfloat vertices[] =
	{
		-0.9f, 0.9f, 0.0f,  0.0f, 1.0f, // vertex 0 : p0.x, p0.y, p0.z, t0.u, t0.v
		0.9f, 0.9f, 0.0f,   1.0f, 1.0f, // vertex 1 : p1.x, p1.y, p1.z, t1.u, t1.v
		0.0f, -0.9f, 0.0f,  0.5f, 0.0f  // vertex 2 : p2.x, p2.y, p2.z, t2.u, t2.v
	};

	
	const int no_vertices = sizeof(vertices_cube) / (sizeof(GLfloat) * (3 + 2));
	const int vertex_stride = sizeof( vertices_cube ) / no_vertices;
	no_of_verticies = no_vertices ;
	// optional index array
	unsigned int indices[] = { 
	 0, 1, 2,   2, 3, 0,    // v0-v1-v2, v2-v3-v0 (front)
     4, 5, 6,   6, 7, 4,    // v0-v3-v4, v4-v5-v0 (right)
     8, 9,10,  10,11, 8,    // v0-v5-v6, v6-v1-v0 (top)
    12,13,14,  14,15,12,    // v1-v6-v7, v7-v2-v1 (left)
    16,17,18,  18,19,16,    // v7-v4-v3, v3-v2-v7 (bottom)
    20,21,22,  22,23,20     // v4-v7-v6, v6-v5-v4 (back) 
	};
	

	
	glGenVertexArrays( 1, &vao );
	glBindVertexArray( vao );
	

	glGenBuffers( 1, &vbo ); // generate vertex buffer object (one of OpenGL objects) and get the unique ID corresponding to that buffer
	glBindBuffer( GL_ARRAY_BUFFER, vbo ); // bind the newly created buffer to the GL_ARRAY_BUFFER target
	glBufferData( GL_ARRAY_BUFFER, sizeof( vertices_cube ), vertices_cube, GL_STATIC_DRAW ); // copies the previously defined vertex data into the buffer's memory
	// vertex position
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void*) 0 );
	glEnableVertexAttribArray( 0 );
	// vertex texture coordinates
	glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, vertex_stride, ( void* )( sizeof( float ) * 3 ) );
	glEnableVertexAttribArray( 1 );
	


	glGenBuffers( 1, &ebo );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ebo );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( indices ), indices, GL_STATIC_DRAW );



}


int loadMaterialToBuffer(Material * material) {


	return 0;
}

void Rasterizer::LoadScene(string file_name) {


	vector<GLfloat> vertex_data = {};
	vector<GLfloat> normals_data = {};

	vector<Surface *> surfaces;
	vector<Material *> materials;

	LoadOBJ(file_name.c_str(), surfaces, materials, false, {0, 0, 0});

	no_of_verticies = 0;

	for (int i = 0; i < surfaces.size(); i++) {
		Surface * surface = surfaces.at(i);

		Material * material = surface->get_material();
		int materialBufferId = loadMaterialToBuffer(material);



		Color3f ambient = material->ambient_;
		Color3f diffuse = material->diffuse_;
		Color3f specular = material->specular_;

		int no_triangles = surface->no_triangles();
		Triangle * triangles = surface->get_triangles();

		for (int j = 0; j < no_triangles; j++) {
			Triangle  triangle = triangles[j];

			for (int k = 0; k < 3; k++) {
				Vertex vertex = triangle.vertex(k);

				// Push position data
				vertex_data.push_back(vertex.position.x);
				vertex_data.push_back(vertex.position.y);
				vertex_data.push_back(vertex.position.z);

				// Push normals
				vertex_data.push_back(vertex.normal.x);
				vertex_data.push_back(vertex.normal.y);
				vertex_data.push_back(vertex.normal.z);

				// Push color
				vertex_data.push_back(vertex.color.x);
				vertex_data.push_back(vertex.color.y);
				vertex_data.push_back(vertex.color.z);

				// Push texture coords
				vertex_data.push_back(vertex.texture_coords->u);
				vertex_data.push_back(vertex.texture_coords->v);

				vertex_data.push_back(ambient.data[0]);
				vertex_data.push_back(ambient.data[1]);
				vertex_data.push_back(ambient.data[2]);

				vertex_data.push_back(diffuse.data[0]);
				vertex_data.push_back(diffuse.data[1]);
				vertex_data.push_back(diffuse.data[2]);

				vertex_data.push_back(specular.data[0]);
				vertex_data.push_back(specular.data[1]);
				vertex_data.push_back(specular.data[2]);

				vertex_data.push_back(material->roughness_);

				no_of_verticies++;
			}
		}
	}

	//GLfloat vertices[vertex_data.size()];
	//for (int i = 0; i < vertex_data.size(); i++)
	//	vertices[i] = vertex_data.at(i);
	GLfloat * vertices = vertex_data.data();


	printf("Loaded %d verticies & uv`s\n",  no_of_verticies);

	// optional index array
	unsigned int indices[no_of_verticies];
	for (int i = 0; i < no_of_verticies; i++)
		indices[i] = i;


//TODO			 ___________________________________________________  __________________________________________________
//TODO			|				VERTEX 1							||				VERTEX 2							|
//TODO			 ___________________________________________________  __________________________________________________
//TODO	BYTE:	|	POSITION XYZ	|	UV COORS	|	NORMAL XYZ	||	POSITION XYZ	|	UV COORS	|	NORMAL XYZ	|
//TODO	DATA: 	|0				  12|			  20|			  32||				  44|			  52|			  64|			  
//TODO	STRIDE: |-------------------|---------------|---------------> 					|				|
//TODO			|					|---------------|---------------------------------->|				|
//TODO			|					|				|--------------- stride 8 * 4 bytes --------------->|
//TODO	UV:		|------------------>| offset 4 * 8 bytes
//TODO	NORM: 	|---------------------------------->| offset 6 * 8 bytes

	const int vertex_size = sizeof( float );
	const int vertex_data_size = vertex_size * vertex_data.size();
	const int vertex_stride = vertex_data_size/ no_of_verticies;	
	printf("Vertex stride: %d\n", vertex_stride);
	const int normal_offset = 3 * sizeof( float );
	const int color_offset = normal_offset + 3 * sizeof( float );
	const int uv_offset = color_offset + 3 * sizeof( float );

	const int ambient_offset = uv_offset + 2 * sizeof( float );
	const int diffuse_offset = ambient_offset + 3 * sizeof( float );
	const int specular_offset = diffuse_offset + 3 * sizeof( float );
	const int roughness_offset = specular_offset + 3 * sizeof( float );
	
	glGenVertexArrays( 1, &vao );
	glBindVertexArray( vao );
	

	glGenBuffers( 1, &vbo ); // generate vertex buffer object (one of OpenGL objects) and get the unique ID corresponding to that buffer
	glBindBuffer( GL_ARRAY_BUFFER, vbo ); // bind the newly created buffer to the GL_ARRAY_BUFFER target
	glBufferData( GL_ARRAY_BUFFER, vertex_data_size, vertex_data.data(), GL_STATIC_DRAW ); // copies the previously defined vertex data into the buffer's memory
	


	// vertex position
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, vertex_stride, 0 );

	// vertex normal
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void *) normal_offset );

	// vertex color
	glEnableVertexAttribArray( 2 );
	glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void *) color_offset );

	// vertex uv
	glEnableVertexAttribArray( 3 );
	glVertexAttribPointer( 3, 2, GL_FLOAT, GL_FALSE, vertex_stride, (void *) uv_offset );


	// triangle ambient
	glEnableVertexAttribArray( 4 );
	glVertexAttribPointer( 4, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void *) ambient_offset );

	// triangle diffuse
	glEnableVertexAttribArray( 5 );
	glVertexAttribPointer( 5, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void *) diffuse_offset );

	// triangle specular
	glEnableVertexAttribArray( 6 );
	glVertexAttribPointer( 6, 3, GL_FLOAT, GL_FALSE, vertex_stride, (void *) specular_offset );

	// material roughness
	glEnableVertexAttribArray( 7 );
	glVertexAttribPointer( 7, 1, GL_FLOAT, GL_FALSE, vertex_stride, (void *) roughness_offset );

	
	
	


	glGenBuffers( 1, &ebo );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ebo );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( indices ), indices, GL_STATIC_DRAW );



}





int Rasterizer::MainLoop() {

	Camera camera = Camera(1280, 720, 0.75, {-20, -30, 100}, {0, 0, 0});


	GLuint eye_id = glGetUniformLocation(shader_program, "Eye");

	GLuint m_id = glGetUniformLocation(shader_program, "M");
	GLuint mv_id = glGetUniformLocation(shader_program, "MV");
	GLuint mvn_id = glGetUniformLocation(shader_program, "MVN");
	GLuint mvp_id = glGetUniformLocation(shader_program, "MVP");
	

	// Turn off VSYNC
	
	glfwSwapInterval(0);
	double lastTime = glfwGetTime();
 	int nbFrames = 0;

	double lastDeltaTime = glfwGetTime();
	// main loop
	while ( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwGetKey(window, GLFW_KEY_Q ) != GLFW_PRESS && !glfwWindowShouldClose( window ) ) {

		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
			// printf and reset timer
			std::printf("%f ms/frame\n", 1000.0/double(nbFrames));
			nbFrames = 0;
			lastTime += 1.0;
		}



		glClearColor( 0.2f, 0.3f, 0.3f, 1.0f ); // sc_str()tate setting function
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT ); // state using function

		glUseProgram(shader_program);


		// Handle mouse & keyboard input
		double mouse_x, mouse_y;
		glfwGetCursorPos(window, &mouse_x, &mouse_y);
		camera.HandleMouse(mouse_x, mouse_y);

		double deltaTime = currentTime - lastDeltaTime;
		lastDeltaTime = currentTime;
		bool isTurboShift = (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS);
		// Move forward
		if (glfwGetKey(window, GLFW_KEY_W ) == GLFW_PRESS)
			camera.MoveForward(deltaTime, isTurboShift);
		// Move backward
		if (glfwGetKey(window, GLFW_KEY_S ) == GLFW_PRESS)
			camera.MoveForward(-deltaTime, isTurboShift);
		// Move right
		if (glfwGetKey(window, GLFW_KEY_D ) == GLFW_PRESS)
			camera.MoveRight(deltaTime, isTurboShift);
		// Move left
		if (glfwGetKey(window, GLFW_KEY_A ) == GLFW_PRESS)
			camera.MoveRight(-deltaTime, isTurboShift);

		if (glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS)
			camera.RotateAroundOrigin(glfwGetTime());





		// Update camera state
		camera.Update();
		
		//Send camera Model - View - Projection matrix
		glUniform3fv(eye_id, 1, camera.view_from().data );

		glUniformMatrix4fv(m_id, 1, GL_FALSE, camera.M.data() );
		glUniformMatrix4fv(mv_id, 1, GL_FALSE, camera.MV.data() );
		glUniformMatrix4fv(mvn_id, 1, GL_FALSE, camera.MVN.data() );
		glUniformMatrix4fv(mvp_id, 1, GL_FALSE, camera.MVP.data() );
		//glUniformMatrix4fv(mvp_id, 1, GL_FALSE, &(MVP_glm[0][0]) );


		glBindVertexArray( vao );
		glBindVertexArray( vbo );




		//Draw triangles
		//glDrawArrays( GL_TRIANGLES, 0, no_of_verticies );
		//glDrawArrays( GL_POINTS, 0, no_of_verticies * 3 );
		//glDrawArrays( GL_LINE_LOOP, 0, no_of_verticies * 3 );
		glDrawElements( GL_TRIANGLES, no_of_verticies , GL_UNSIGNED_INT, 0 ); // optional - render from an index buffer

		glfwSwapBuffers( window );
		glfwPollEvents();
	}

	glDeleteShader( vertex_shader );
	glDeleteShader( fragment_shader );
	glDeleteProgram( shader_program );

	glDeleteBuffers( 1, &vbo );
	glDeleteVertexArrays( 1, &vao );

	glfwTerminate();

	printf("Safely terminated.");
	return EXIT_SUCCESS;

}



