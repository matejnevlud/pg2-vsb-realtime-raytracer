#ifndef PCH_H
#define PCH_H

#define NOMINMAX
#define _CRT_SECURE_NO_WARNINGS

typedef unsigned char BYTE;

// std libs
#include <stdio.h>
#include <cstdlib>
#include <string>
#include <cstring>
#include <vector>
#include <map>
#include <random>
#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>
#include <functional>

// Glad - multi-Language GL/GLES/EGL/GLX/WGL loader-generator based on the official specs
//#include <glad/glad.h>
#include <glad.h>





// GLFW - simple API for creating windows, receiving input and events
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>



// GLM - OpenGL Math Library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
using namespace glm;

//#define GLM_CONFIG_CLIP_CONTROL GLM_CLIP_CONTROL_LH_ZO;

using namespace std;
#endif //PCH_H
