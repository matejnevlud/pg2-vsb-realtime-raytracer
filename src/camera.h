#ifndef CAMERA_H_
#define CAMERA_H_
#include "pch.h"
#include "vector3.h"
#include "matrix3x3.h"
#include "matrix4x4.h"

/*! \class Camera
\brief A simple pin-hole camera.

\author Tom� Fabi�n
\version 1.0
\date 2018-2019
*/
class Camera
{
public:
	Camera() { }

	Camera( const int width, const int height, const float fov_y,
		const Vector3 view_from, const Vector3 view_at );

	Vector3 view_from() const;
	Matrix3x3 M_c_w() const;
	float focal_length() const;

	void set_fov_y( const float fov_y );

	void Update();

	void MoveForward( const float dt, bool isTurbo = false );
	void MoveRight( const float dt, bool isTurbo = false );
	void HandleMouse( const float mouse_x, const float mouse_y);

	void RotateAroundOrigin(const float dt);

	//Matrix4x4 MVP();
	mat4 MVP_glm();

	Matrix4x4 M;
	Matrix4x4 MV;
	Matrix4x4 MVP;
	Matrix4x4 MVN;

private:
	int width_{ 640 }; // image width (px)
	int height_{ 480 };  // image height (px)
	float fov_y_{ 0.785f }; // vertical field of view (rad)
	
	Vector3 last_view_from_ = {0.0, 0.0, 0.0}; // ray origin or eye or O
	Vector3 view_from_; // ray origin or eye or O
	Vector3 view_at_; // target T
	Vector3 up_{ Vector3( 0.0f, 0.0f, 1.0f ) }; // up vector
	Vector3 origin_ {0.0, 0.0, 0.0};

	float f_y_{ 1.0f }; // focal lenght (px)

	Matrix3x3 M_c_w_; // transformation matrix from CS -> WS


	float move_speed = 10.0f;
	float turbo_speed = 50.0f;

	// MOUSE VARIABLES
	bool first_mouse_move = true;	
	float last_mouse_x = 0.0f;
	float last_mouse_y = 0.0f;
	float yaw = 0.0f;
	float pitch = 0.0f;

	
};

#endif
