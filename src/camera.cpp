#include "camera.h"


Vector3 Normalize(Vector3 v) {
	const float sqr_norm = v.SqrL2Norm();	

	if ( sqr_norm != 0.0f )
	{
		const float norm = sqrt( sqr_norm );
		const float rn = 1.0f / norm;

		v.x *= rn;
		v.y *= rn;
		v.z *= rn;

		return v;
	}	

	return v;
};


Camera::Camera( const int width, const int height, const float fov_y, const Vector3 view_from, const Vector3 view_at )
{
	width_ = width;
	height_ = height;
	fov_y_ = fov_y;

	view_from_ = view_from;
	last_view_from_ = view_from;
	
	
	
	Vector3 dir = view_at_ - view_from_;
	dir.Normalize();
	view_at_ = view_from + dir;

	//view_at_ = view_at;

	// TODO compute focal lenght based on the vertical field of view and the camera resolution
	
	// TODO build M_c_w_ matrix

	Update();
}

Vector3 Camera::view_from() const
{
	return view_from_;
}

Matrix3x3 Camera::M_c_w() const
{
	return M_c_w_;
}

float Camera::focal_length() const
{
	return f_y_;
}

void Camera::set_fov_y( const float fov_y )
{
	assert( fov_y > 0.0 );

	fov_y_ = fov_y;
}

void Camera::Update()
{
	f_y_ = height_ / ( 2.0f * tanf( fov_y_ * 0.5f ) );

	Vector3 z_c = view_from_ - view_at_;
	z_c.Normalize();
	Vector3 x_c = up_.CrossProduct( z_c );
	x_c.Normalize();
	Vector3 y_c = z_c.CrossProduct( x_c );
	y_c.Normalize();
	M_c_w_ = Matrix3x3( x_c, y_c, z_c );




	// Model matrix : an identity matrix (model will be at the origin)
	Matrix4x4 model = Matrix4x4();
	M = model;



	// View matrix
	Vector3 z_e = view_from_ - view_at_;
	z_e.Normalize();

	Vector3 x_e = up_.CrossProduct(z_e);
	x_e.Normalize();

	Vector3 y_e = z_e.CrossProduct(x_e);
	y_e.Normalize();

	Matrix4x4 view = Matrix4x4(x_e, y_e, z_e, view_from_);
	view.EuclideanInverse();



	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units

	float n = 0.1f;
	float f = 10000.0f;

	float aspect = (float) width_ / (float)height_;
	float fov_x = 2 * atanf(tanf(fov_y_ / 2.0f) * aspect);

	float h = 2 * n * tanf(fov_y_ / 2.0f);
	float w = h * aspect; // 2 * n * tanf(fov_x / 2.0f);

	float a = (n + f) / (n - f);
	float b = (1.0f * n * f) / (n - f);

	Matrix4x4 N = Matrix4x4(
		2.0f / w, 		0.0f, 			0.0f, 			0.0f,
		0.0f, 			2.0f / h, 		0.0f, 			0.0f,
		0.0f, 			0.0f, 			1.0f, 			0.0f,
		0.0f, 			0.0f, 			0.0f, 			1.0f
	);

	Matrix4x4 M = Matrix4x4(
		n,		 		0.0f, 			0.0f, 			0.0f,
		0.0f, 			n,			 	0.0f, 			0.0f,
		0.0f, 			0.0f,	 		a, 				b,
		0.0f, 			0.0f, 			-1.0f, 			0
	);

	Matrix4x4 projection = N * M;
	

	MV = view * model;

	MVN = view * model;
	MVN.EuclideanInverse();
	MVN.Transpose();

	MVP = projection * MV;
	MVP.Transpose();

	

	//printf("Camera: (%.03f)\n", );
	if(!last_view_from_.CompareInt(view_from_)) {
		printf("Camera: (%d, %d, %d)\n", int(view_from_.x), int(view_from_.y), int(view_from_.z));
		last_view_from_ = view_from_;	
	}
//	return Matrix4x4(
//		-1.189016, -1.046731, -0.371465, -0.371391,
//		0.792677, -1.570096, -0.557197, -0.557086,
//		0.000000, 1.700937, -0.742930, -0.742781,
//		0.000000, 0.000000, 53.762409, 53.851654
//	);

}


void Camera::MoveForward( const float dt, bool isTurbo )
{
	Vector3 ds = view_at_ - view_from_;
	ds.Normalize();
	ds *= isTurbo ? dt * turbo_speed : dt * move_speed;

	view_from_ += ds;
	view_at_ += ds;
}

void Camera::MoveRight( const float dt, bool isTurbo )
{

	Vector3 dir = view_at_ - view_from_;
	dir.Normalize();

	Vector3 right = dir.CrossProduct(up_);
	right.Normalize();
	right *= isTurbo ? dt * turbo_speed : dt * move_speed;

	view_from_ += right;
	view_at_ += right;
}

void Camera::HandleMouse( const float mouse_x, const float mouse_y) {

	Vector3 view_dir = view_at_ - view_from_;
	view_dir.Normalize();

	if(first_mouse_move) {
		last_mouse_x = mouse_x;
		last_mouse_y = mouse_y;
		first_mouse_move = false;

		yaw = 90;
		pitch = 0;
	}

	float xoffset = last_mouse_x - mouse_x;
    float yoffset = last_mouse_y - mouse_y; 
    last_mouse_x = mouse_x;
    last_mouse_y = mouse_y;

    float sensitivity = 0.1f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw   += xoffset;
    pitch += yoffset;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    Vector3 new_view_dir;
    new_view_dir.x = cos(radians(pitch)) * cos(radians(yaw));
    new_view_dir.z = sin(radians(pitch));
    new_view_dir.y = cos(radians(pitch)) * sin(radians(yaw));

    view_at_ = Normalize(new_view_dir) + view_from_;
}


void Camera::RotateAroundOrigin(const float dt)
{

	const float radius = 200.0f;
	float camX = sin(dt) * radius;
	float camY = cos(dt) * radius;
	
	view_at_ = origin_;
	view_from_ = {camX, camY, 200};

}



void printMatrixGLM( const mat4 & mat ) {
	string proj = 
	std::to_string( mat[0][0] ) + " " + std::to_string( mat[1][0] ) + " " + std::to_string( mat[2][0] ) + " " + std::to_string( mat[3][0] ) + "\n" +
	std::to_string( mat[0][1] ) + " " + std::to_string( mat[1][1] ) + " " + std::to_string( mat[2][1] ) + " " + std::to_string( mat[3][1] ) + "\n" +
	std::to_string( mat[0][2] ) + " " + std::to_string( mat[1][2] ) + " " + std::to_string( mat[2][2] ) + " " + std::to_string( mat[3][2] ) + "\n" +
	std::to_string( mat[0][3] ) + " " + std::to_string( mat[1][3] ) + " " + std::to_string( mat[2][3] ) + " " + std::to_string( mat[3][3] ) + "\n";
	printf(proj.c_str());
	
}

void testMatrix() {
	Matrix4x4 test = Matrix4x4(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);

	mat4 testglm = make_mat4(test.data());
	//mat4 testglm = glm::mat4( 1.0 );
	
	printf(test.toString().c_str());
	printf("\n");
	
	//printf(glm::to_string(testglm).c_str());
	printMatrixGLM(testglm);
	printf("\n");
}


mat4 Camera::MVP_glm() {
	

	return mat4(
		-1.189016, 0.792677, 0.000000, 0.000000,
		-1.046731, -1.570096, 1.700937, 0.000000,
		-0.371465, -0.557197, -0.742930, 53.662399,
		-0.371391, -0.557086, -0.742781, 53.851654
	);
	// Model matrix : an identity matrix (model will be at the origin)
	Matrix4x4 model = Matrix4x4();
	glm::mat4 Model = make_mat4(model.data());




	glm::mat4 View = glm::lookAt(
		glm::vec3(view_from_.x, view_from_.y, view_from_.z), // Camera is at (4,3,3), in World Space
		glm::vec3(view_at_.x, view_at_.y, view_at_.z), // and looks at the origin
		glm::vec3(up_.x, up_.y, up_.z)  // Head is up (set to 0,-1,0 to look upside-down)
	);

	

	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(fov_y_, (float) width_ / (float)height_, 0.1f, 1000.0f);
	//mat4 Projection = make_mat4(projection.data());
	

	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 mv = View * Model; // Remember, matrix multiplication is the other way around <--
	glm::mat4 mvp = Projection * View * Model; // Remember, matrix multiplication is the other way around <--	

	printf("View matice - glm\n");
	printMatrixGLM(View);
	printf("\n");

	printf("Projekční matice - glm\n");
	printMatrixGLM(Projection);
	printf("\n");

	printf("MVP matice - glm\n");
	printMatrixGLM(mvp);
	printf("\n");



	return mvp;
	
}

/*
mat4 MVP_glmOPA() {
	
	// Model matrix : an identity matrix (model will be at the origin)
	glm::mat4 Model = glm::mat4(1.0f);

	// Camera matrix
	glm::mat4 View = glm::lookAt(
		glm::vec3(view_from_.x, view_from_.y, view_from_.z), // Camera is at (4,3,3), in World Space
		glm::vec3(view_at_.x, view_at_.y, view_at_.z), // and looks at the origin
		glm::vec3(up_.x, up_.y, up_.z)  // Head is up (set to 0,-1,0 to look upside-down)
		);


	// Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	glm::mat4 Projection = glm::perspective(fov_y_, (float) width_ / (float)height_, 0.1f, 1000.0f);
	
	// Or, for an ortho camera :
	//glm::mat4 Projection = glm::ortho(-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f); // In world coordinates
	
	
	// Our ModelViewProjection : multiplication of our 3 matrices
	glm::mat4 mv = View * Model; // Remember, matrix multiplication is the other way around <--
	glm::mat4 mvp = Projection * mv; // Remember, matrix multiplication is the other way around <--


	printf("\nproj - glm\n");
	printMatrixGLM(Projection);
	
	printf("\nMVP - glm\n");
	printMatrixGLM(mvp);
	
	return mvp;

	printf("\nmodel - glm\n");
	printMatrixGLM(Model);
	
	printf("\nview - glm\n");
	printMatrixGLM(View);
	
	printf("\nproj - glm\n");
	printMatrixGLM(Projection);
	
}
*/