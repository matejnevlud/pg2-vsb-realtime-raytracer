#
# Cross Platform Makefile
# Compatible with MSYS2/MINGW, Ubuntu 14.04.1 and Mac OS X
#
# You will need GLFW (http://www.glfw.org):
# Linux:
#   apt-get install libglfw-dev
# Mac OS X:
#   brew install glfw
# MSYS2:
#   pacman -S --noconfirm --needed mingw-w64-x86_64-toolchain mingw-w64-x86_64-glfw
#

#CXX = g++
CXX = clang++

BREW_DIR = /opt/homebrew
UNAME_P := $(shell uname -p)
ifeq ($(UNAME_P), i386)
	BREW_DIR = /usr/local
endif
ifeq ($(UNAME_P), arm)
	BREW_DIR = /opt/homebrew
endif


LIBGLFW = /opt/homebrew/opt/glfw
LIBGLM = /opt/homebrew/Cellar/glm/0.9.9.8
LIBFREEIMAGE = /opt/homebrew/opt/freeimage
LIBOPENMP = /opt/homebrew/opt/libomp
LIBEMBREE = /Users/matejnevlud/local/embree3


EXE = raytracer.run
SOURCES = $(wildcard ./src/*.cpp)
SOURCES += ./libs/imgui/imgui_impl_glfw.cpp ./libs/imgui/imgui_impl_opengl2.cpp
SOURCES += ./libs/imgui/imgui.cpp ./libs/imgui/imgui_demo.cpp ./libs/imgui/imgui_draw.cpp ./libs/imgui/imgui_widgets.cpp
SOURCES += src/glad.c
#SOURCES += ./libs/glad/src/glad.cpp
OBJS = $(addsuffix .o, $(basename $(notdir $(SOURCES))))
UNAME_S := $(shell uname -s)

CXXFLAGS = -I./libs/imgui/
CXXFLAGS += -g -Wall -Wformat -std=c++11 -stdlib=libc++ -Wc++11-extensions 
LIBS = 

##---------------------------------------------------------------------
## BUILD FLAGS PER PLATFORM
##---------------------------------------------------------------------

ifeq ($(UNAME_S), Linux) #LINUX
	ECHO_MESSAGE = "Linux"
	LIBS += -lGL `pkg-config --static --libs glfw3`

	CXXFLAGS += `pkg-config --cflags glfw3`
	CFLAGS = $(CXXFLAGS)
endif

ifeq ($(UNAME_S), Darwin) #APPLE
	ECHO_MESSAGE = "Mac OS X"
	
	
	LIBS += -framework OpenGL -framework Cocoa -framework IOKit -framework CoreVideo
	LIBS += -lglfw -lfreeimage -lembree3 -ldl
	LIBS += -L/usr/local/lib -L/opt/local/lib 
	LIBS += -L$(LIBGLFW)/lib
	LIBS += -L$(LIBEMBREE)/lib
	LIBS += -L$(LIBFREEIMAGE)/lib
	LIBS += -L$(LIBOPENMP)/lib
	LIBS += -L$(LIBGLM)/lib
	
	
	
	CXXFLAGS += -Xpreprocessor -fopenmp -lomp 
	CXXFLAGS += -I/usr/local/include -I/opt/local/include
	CXXFLAGS += -I$(LIBGLFW)/include
	CXXFLAGS += -I$(LIBEMBREE)/include
	CXXFLAGS += -I$(LIBFREEIMAGE)/include
	CXXFLAGS += -I$(LIBOPENMP)/include
	CXXFLAGS += -I$(LIBGLM)/include
	CXXFLAGS += -I./libs/glad
	CXXFLAGS += -I./libs/khr


	CFLAGS = $(CXXFLAGS)
endif

ifeq ($(findstring MINGW,$(UNAME_S)),MINGW)
	ECHO_MESSAGE = "MinGW"
	LIBS += -lglfw3 -lgdi32 -lopengl32 -limm32

	CXXFLAGS += `pkg-config --cflags glfw3`
	CFLAGS = $(CXXFLAGS)
endif

##---------------------------------------------------------------------
## BUILD RULES
##---------------------------------------------------------------------

%.o:./src/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o:./src/%.c
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o:./libs/imgui/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<


all: $(EXE)
	@echo OPA
	@echo Build complete for $(ECHO_MESSAGE)

$(EXE): $(OBJS)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBS)

clean:
	@echo $(UNAME_P)
	rm -f $(OBJS)