#version 410 core
#define PI 3.1415926538

in vec4 wPosition;
in vec3 Position;
in vec3 Normal;
in vec3 Color;
in vec2 TexCoord;

in vec3 Ambient;
in vec3 Diffuse;
in vec3 Specular;

in float Roughness;


uniform sampler2D irradianceMap;
uniform sampler2D ggxMap;
uniform sampler2D environmentMap;


uniform vec3 Eye;

uniform mat4 M;
uniform mat4 MV;
uniform mat4 MVN;
uniform mat4 MVP;


out vec4 FragColor;


vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float rough)
{
    return F0 + (max(vec3(1.0 - rough), F0) - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}  


vec4 sample_irradianceMap(vec3 dir) {
	vec2 uv;
	uv.x = atan( dir.x, dir.y );
	uv.y = acos( dir.z );
	uv /= vec2( 2 * PI, PI );
	
 	return texture( irradianceMap, uv);
}

vec4 sample_irradianceMap2(vec3 dir) {
	vec2 uv;
	uv.x = atan( dir.x, dir.y );
	uv.y = asin( dir.z );
	uv *= vec2(0.1591, 0.3183);
	uv += 0.5;
	
 	return texture( irradianceMap, uv);
}

vec4 sample_ggxMap(vec3 dir) {
	vec2 uv;
	uv.x = atan( dir.x, dir.y );
	uv.y = acos( dir.z );
	uv /= vec2( 2 * PI, PI );
	
 	return texture( ggxMap, uv);
}

vec4 sample_environmentMap(vec3 dir, float alfa) {
	vec2 uv;
	uv.x = atan( dir.x, dir.y );
	uv.y = acos( dir.z );
	uv /= vec2( 2 * PI, PI );
	
	float lod = alfa * 7;
 	return textureLod( environmentMap, uv, lod);
}



vec3 tone_mapping (vec3 color) {
	const float gamma = 2.2;
  
    // reinhard tone mapping
    vec3 mapped = color / (color + vec3(1.0));

    // gamma correct
    mapped = pow(mapped, vec3(1.0 / gamma)); 

	return mapped;
}


void main( void ) {


	vec3 N = Normal;
    vec3 V = normalize(Eye - Position);
    vec3 R = reflect(-V, N); 

    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)    
    vec3 F0 = vec3(0.04); 
    //F0 = mix(F0, Diffuse);
	vec3 Lo = vec3(0, 0, 0);

	// ambient lighting (we now use IBL as the ambient term)
    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, Roughness);
    vec3 kS = F;
    vec3 kD = 1.0 - kS;
	//kD *= 1.0 - metallic;
	
	// Diffuse part
    vec3 diffuse = sample_irradianceMap(N).rgb * (Diffuse / PI);
	Lo += kD * diffuse;

	// Specular part
    vec3 prefilteredColor = sample_environmentMap(R, Roughness).rgb;    
    vec2 brdf = texture(ggxMap, vec2(max(dot(N, V), 0.0), Roughness)).rg;
    vec3 specular = prefilteredColor * (F * brdf.x + brdf.y);
	Lo += specular;

	FragColor = vec4( tone_mapping(Lo), 1.0 );


	//FragColor = vec4(sample_ggxMap(Normal).rgb, 1.0);
	FragColor = vec4(sample_environmentMap(Normal, 0.1).rgb, 1.0);
	//FragColor = vec4( normalize(Normal) * 0.5 + 0.5, 1.0 );
	//FragColor = vec4( sample_irradianceMap2(N).rgb * (Diffuse / PI), 1.0 );
}
